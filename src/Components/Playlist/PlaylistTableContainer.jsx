import React from "react";
import PlaylistTable from "./PlaylistTable";

const PlaylistTableContainer = props => {
  const { playlists } = props;
  return <PlaylistTable playlists={playlists} />;
};

export default PlaylistTableContainer;
