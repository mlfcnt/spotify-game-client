import React from "react";

const Title = props => {
  return (
    <div>
      <p>Know your playlist</p>
      {props.connected ? (
        <div>
          <p style={{ display: "inline" }}> Connecté en tant que ... </p>
          <a style={{ display: "inline" }} href="http://localhost:8888/login">
            Changer de compte
          </a>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default Title;
