import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Main from "./Main";

function App() {
  const getPlaylists = () => {
    fetch("https://api.spotify.com/v1/me/playlists")
      .then(response => response.json())
      .then(data => console.log(data));
  };

  return <Main />;
}

export default App;
