import React, { useEffect, useState } from "react";
import PlaylistTableContainer from "./Components/Playlist/PlaylistTableContainer";
import Title from "./Components/Title/Title";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import queryString from "query-string";
import Grid from "@material-ui/core/Grid";
import Footer from "./Components/Footer/Footer";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  },
  footer: {
    align: "center",
    color: "red"
  }
}));

const Main = () => {
  const [playlists, setPlaylists] = useState([]);
  const [connected, isConnected] = useState(false);
  useEffect(() => {
    let parsed = queryString.parse(window.location.search);
    let accessToken = parsed.access_token;
    let username = "";

    fetch("https://api.spotify.com/v1/me/playlists", {
      headers: {
        Authorization: "Bearer " + accessToken
      }
    })
      .then(response => response.json())
      .then(data => setPlaylists(data.items));
  }, []);
  const classes = useStyles();
  return (
    <div>
      <Container maxWidth="xl" id="containerMain">
        <Grid
          container="sm"
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Title connected={connected} />
          {playlists ? (
            <PlaylistTableContainer playlists={playlists} />
          ) : (
            <div>
              <p>Vous devez être connecté à Spotify pour jouer</p>
              <Button
                variant="contained"
                className={classes.button}
                onClick={() =>
                  (window.location = "http://localhost:8888/login")
                }
              >
                Se connecter
              </Button>
            </div>
          )}
        </Grid>
      </Container>
      <Footer className={classes.footer} />
    </div>
  );
};

export default Main;
